<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Login extends CI_Controller {

        function __construct()
        {
            parent::__construct();
            $this->load->library("session");
            $this->load->library('mongo_db');
        }

        function index($msg = NULL)
        {
            $data['msg'] = $msg;
            $this->load->view('login',$data);
        }

        function process()
        { 
            $username = $this->security->xss_clean($this->input->post('username')); 
            $password = $this->security->xss_clean($this->input->post('password'));
            
          // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => 'http://api.audiocompass.in/auth/token',
                    CURLOPT_USERAGENT => 'Codular Sample cURL Request',
                    CURLOPT_POST => 1,
                    CURLOPT_POSTFIELDS => array(
                        'username' => $username,
                        'password' => $password
                    )
                ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            $str_resp = explode(":",$resp); 
            $str_resp1 = str_replace('"','',$str_resp[1]);
            $str_resp2 = str_replace('"}','',$str_resp1);
            $data = str_replace('}','',$str_resp2);
            curl_close($curl);
            if(substr($data,0,3)=='404'){
                $msg = 'Invalid username and/or password.';
                $this->index($msg); 
                return false;
            }
            else{
                $userData = array(
                        'username' => $username,
                        'token' => $data,
                        'isLoggedIn'=>true
                    );
                $this->session->set_userdata($userData);
                redirect('trips');
            }
            
        }

        function logout_user()
        {
            $data = array(
                'name' => '',
                'token' => '',
                'isLoggedIn'=>false
            ); 

            $this->session->unset_userdata($data);
            $this->session->sess_destroy();

            redirect('/login','refresh');
        }

    }

?>
