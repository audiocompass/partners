<!-- daterange picker -->
<link href="<?php echo base_url('assets/css/daterangepicker/daterangepicker-bs3.css'); ?>" rel="stylesheet" type="text/css" />
<div class="col-md-12">
    <div class="row">
        <!-- left column -->
        <div class="col-md-4 col-md-offset-4">
            <legend>Add a trip</legend>
            <form role="form" name="trip-form" method="post" action="<?php echo base_url('index.php/trips/save') ?>">
                <div class="form-group">
                    <label for="pnr_code">Booking Id <font color="red">*</font></label>
                    <input type="text" class="form-control" id="pnr_code" name="pnr_code" placeholder="Booking Id" required="required">
                </div>
                <div class="form-group">
                    <label for="email">Passenger Email Address <font color="red">*</font></label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address" required="required">
                </div>
                <div class="form-group">
                    <label for="fname">Passenger First Name <font color="red">*</font></label>
                    <input type="text" class="form-control" id="fname" name="fname" placeholder="Enter first name" required="required">
                </div>
                <div class="form-group">
                    <label for="lname">Passenger Last Name <font color="red">*</font></label>
                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Enter last name" required="required">
                </div>
                <div class="form-group">
                    <label for="mobile">Passenger Mobile Number(10 digit number) <font color="red">*</font></label> 
                    <div class="input-group">
                        <div class="input-group-addon" style="padding: 0px 8px;">
                            <p class="help-block" style="padding-top: 3px;">+91</p>
                        </div>
                        <input type="text" class="form-control pull-right" id="mobile" name="mobile" placeholder="Enter 10 digit mobile no." required="required" pattern="[789][0-9]{9}">
                    </div>
                </div>
                <div class="form-group">
                    <label>Trip Dates <font color="red">*</font></label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="reservation" name="reservation" required="required"/>
                    </div><!-- /.input group -->
                </div><!-- /.form group -->
                <div class="form-group">
                <label for="destination">Destination separated by a comma.(e.g: Mumbai,Pune)</label>
                    <input type="text" class="form-control" id="destination" name="destination" placeholder="Enter destination">
                    <p class="help-block"></p>
                </div>
                <div class="form-group">
                    <label for="no_active">Number of Passengers <font color="red">*</font></label>
                    <select class="form-control" id="no_active" name="no_active" required="required">
                        <option value="1">1</option>
                        <option value="2" selected="true">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                    </select>
                </div>
                <p>
                    <input type="submit" class="btn btn-primary" value="Submit"></input>
                    <input type="button" name="cancel" value="Cancel" class="btn btn-primary" onclick="javascript:location.href='<?php echo base_url(); ?>/index.php/trips'">
                </p>
            </form>
        </div>
