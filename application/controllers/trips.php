<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trips extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library("session");
        $this->load->library('mongo_db');
    }

    function index($msg = NULL)
    { 
        ini_set('display_errors', 'Off');

        $this->load->view('include/header');
            // Get cURL resource
        $curl = curl_init();
            // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://api.audiocompass.in/trips',
            CURLOPT_USERAGENT => 'cURL Request',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array( 'access_token' => $this->session->userdata("token"))
            ));
            // Send the request & save response to $resp
        $resp = curl_exec($curl); 
            /* $name = $this->session->userdata("name");                                                         
            $result = $this->mongo_db->where(array('name' => $name))->get('agents');
            foreach($result as $row)
            { $id = $row['_id']; }

            $this->load->view('include/header');
            //$this->load->view('include/sidebar');
            //$result = $this->mongo_db->where(array('user_id' => $id))->order_by(array('trip_start' => 'DESC'))->get('trips');
            $result = $this->mongo_db->order_by(array('trip_start' => 'DESC'))->get('trips'); */
            $this->load->view('manage', array('data' => $resp));
            // Close request to clear up some resources
            curl_close($curl);
            $this->load->view('include/footer1'); 
        }

        function add()
        { 
            $this->load->view('include/header');
            //$this->load->view('include/sidebar');
            $this->load->view('trip');
            $this->load->view('include/footer');
        }

        function save()
        {
            $name = $this->session->userdata("name");  
            // print_r($this->session->userdata("token"));
            // exit;                                                      
            $result = $this->mongo_db->where(array('name' => $name))->get('agents');
            foreach($result as $row)
                { $id = $row['_id']; } 

            $trip_duration = $this->input->post('reservation');
            $str_trip_duration = explode('-',$trip_duration);
            $str_trip_start = $str_trip_duration[0];
            $trip_start = date("Y-m-d", strtotime($str_trip_start));
            $trip_start_datetime = new DateTime($trip_start);

            $str_trip_end = $str_trip_duration[1];
            $trip_end = date("Y-m-d", strtotime($str_trip_end));
            $trip_end_datetime = new DateTime($trip_end);

            $datetime = new DateTime(date('Y-m-d H:i:s'));

            header('Content-type: application/json');
            $destOutput = array();
            $destArry = array();
            $trip_dest = explode(',',$this->input->post('destination'));
            $tmp  = '';
            for($i=0;$i<count($trip_dest);$i++)
            {
                $destArry[$i] = $trip_dest[$i]; 
                $tmp .= "\"{$trip_dest[$i]}\",";
            }
            $destOutput[] = $destArry;
            $tmp = substr($tmp,0,strlen($tmp)-1);

            $insert_data = array('access_token' => $this->session->userdata("token"),
                'pnr' => $this->input->post('pnr_code'),
                'email' => $this->input->post('email'),
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'mobile' => $this->input->post('mobile'),
                'trip_start' => $trip_start,
                'trip_end' => $trip_end,
                'destinations' => '['.$tmp.']',
                'num_of_activations' => (int)$this->input->post('no_active')
                );

            // Get cURL resource

            // exit;
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://api.audiocompass.in/trips/create',
                CURLOPT_USERAGENT => 'cURL Request',
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $insert_data
                ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);
            $newResponse = json_decode($resp);
            if(!array_key_exists('statusCode', $newResponse))
            {           
                $this->session->set_flashdata('error', 'Trip has been added successfully.');
                redirect("trips");
            }
            else{
                $this->session->set_flashdata('error', 'Trip was not created, Please try again.');
                redirect("trips");
            }
        }

        function delete()
        {
            $id = $this->input->get('id');

            // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => 'http://api.audiocompass.in/trips/delete',
                CURLOPT_USERAGENT => 'cURL Request',
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => array( 
                    'id' => $id,
                    'access_token' => $this->session->userdata("token"))
                ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);

            $this->session->set_flashdata('error', 'Trip has been deleted successfully.');
            redirect("trips");
        }
    }

    ?>
