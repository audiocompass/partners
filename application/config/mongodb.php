<?php
    //mongodb host
    //$config['default']['mongo_hostbase'] = '192.168.1.50';
    //mongodb name
    //$config['default']['mongo_database'] = 'admin';
    //mongodb username - by default, it is empty
    //$config['default']['mongo_username'] = 'admin';
    //mongodb password - by default, it is empty
    //$config['default']['mongo_password'] = 'pass123456';

    $config['default']['mongo_hostbase'] = '54.254.139.125:27017';
    $config['default']['mongo_database'] = 'content';
    $config['default']['mongo_username'] = '';
    $config['default']['mongo_password'] = '';
    $config['default']['mongo_persist']  = FALSE;
    $config['default']['mongo_persist_key']     = 'ci_persist';
    $config['default']['mongo_replica_set']  = FALSE;
    $config['default']['mongo_query_safety'] = 'safe';
    $config['default']['mongo_suppress_connect_error'] = TRUE;
    $config['default']['mongo_host_db_flag']   = FALSE;
?>