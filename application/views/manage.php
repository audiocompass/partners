<!-- DATA TABLES -->
<link href="<?php echo base_url('assets/css/datatables/dataTables.bootstrap.css');?>" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<!--<link href="<?php echo base_url('assets/css/AdminLTE.css');?>" rel="stylesheet" type="text/css" />-->

<div class="col-md-12">
    <div class="row">
      <div class="col-md-4">
        <h4>
            <a href="<?php echo base_url('index.php/trips/add');?>" class="btn btn-primary btn-large" title="add trip">Add Trip</a>
        </h4>
    </div>
    <div class="col-md-6"><h3>Agent dashboard</h3></div>

</div>
<div class="row">
    <?php $error_msg = $this->session->flashdata('error');
    if(!empty($error_msg)){ ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b>&nbsp;<?php echo $error_msg;?>
    </div>
    <?php } ?>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Booking Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email Address</th>
                <th>Mobile</th>
                <th>No. of Pax</th>
                <th>Trip Start Date</th>
                <th>Trip End Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $j=0;
            $count=0;
            $result = json_decode($data);
            foreach($result as $key => $value)
            { 
                if(!empty($value->pnr))
                {  
                    $trip_start = date("d M Y", strtotime($value->trip_start));
                    $trip_end = date("d M Y", strtotime($value->trip_end));
                    $count++;
                    ?>
                    <tr>
                        <td><?php echo $value->pnr;?></td>
                        <td><?php echo $value->fname;?></td>
                        <td><?php echo $value->lname;?></td>
                        <td><?php echo $value->email;?></td>
                        <td><?php echo $value->mobile;?></td>
                        <td><?php echo $value->pax?></td>
                        <td><?php echo $trip_start;?></td>
                        <td><?php echo $trip_end;?></td>
                        <td><a href="<?php echo base_url('index.php/trips/delete?id='.$value->_id);?>"><button class="btn btn-success">Delete</button></a></td>
                    </tr>
                    <?php } $j++;}  ?>
                    <!-- <small class="badge pull-left bg-green"><?php echo $count; ?>&nbsp;Trips</small><br /> <br /> -->
                </tbody>
            </table>
        </div>
    </div>