<?php 
if($this->session->userdata("token")=='')
{
    redirect('/');    
}
?>   
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>AudioCompass | <?php echo uri_string()?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?php echo base_url('assets/css/ionicons.min.css');?>" rel="stylesheet" type="text/css" />
</head>
<body class="skin-blue">
    <header class="header">
        <nav class="navbar navbar-inverse" style='border-radius:0px;' role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">

                  <a class="navbar-brand" href="<?php echo base_url('index.php/trips'); ?>">AudioCompass</a>
              </div>
              <div class="collapse navbar-collapse pull-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i>
                            <span> <?php echo $this->session->userdata("username"); ?><i class="caret"></i></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                    <a href="<?php echo base_url('index.php/login/logout_user'); ?>">Sign out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
